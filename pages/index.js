Page({
  data: {
    items: [
      {
        title: '分段组件',
        url: './radiogroup/index',
      },
      {
        title: '弹窗',
        url: './alert/index',
      },
      {
        title: '城市切换',
        url: './city/index',
      },
      {
        title: '聊天界面',
        url: './chat/index',
      },
      {
        title: '购物车',
        url: './cart/index',
      },
      {
        title: '底部Tab',
        url: './tab/index',
      },
      {
        title: '时间轴',
        url: './timeline/index',
      }
    ]
  }
})